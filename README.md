# radio shack meter interface

Radio Shack offered a multimeter with an RS-232 serial data interface implemented with a DE-9 connector.  The available software was DOS-only, and the protocol is a little silly, so I wrote up a quick parser for it.

The meter part number is 220-812, sometimes listed as 22-812.

The protocol just represents which segments are displayed on the LCD, so you have to work backwards from those to get the digits, where the decimal point is, and which annunciators are on, to determine the meter mode (volts, milliamperes, etc.)
